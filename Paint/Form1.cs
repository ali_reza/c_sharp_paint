﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;


namespace Paint
{
    public partial class Form1 : Form
    {
        public static MouseEventArgs eve;
        public static TextBox T1, T2;
        public static Bitmap flag;
        public static Graphics Graphics;
        public static PictureBox PictureBox1;
        public static Color bg = Color.FromArgb(0, 0, 0);
        public static Color color_ = Color.FromArgb(255, 0, 0);
        public static Color bgcolor = Color.FromArgb(0, 0, 0);
        public static Brush black = new SolidBrush(bg);
        public static Brush WI = new SolidBrush(Color.FromArgb(0, 255, 0));

        public static Brush brush = new SolidBrush(Color.FromArgb(255, 0, 0));
        public static Pen pen = new Pen(Color.FromArgb(255, 0, 0));
        public static bool Down = false;
        public static int wher = 0, X_ = 0, Y_ = 0, last_x = 100, last_y = 100;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_MouseHover(object sender, EventArgs e)
        {
            label1.Text = MousePosition.X.ToString();
        }
        
        private int rond(int num)
        {
            if (num < 0)
                num *= (-1);
            return num;
        }

        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            eve = e;
            label1.Text = "X : " + e.X.ToString() + ", Y : " + e.Y.ToString();
            Graphics.FillRectangle(brush, e.X, e.Y, 20, 20);
            PictureBox1.Image = flag;
            PictureBox1.Refresh();
        }

        private void Click(object sender, MouseEventArgs e)
        {
            label1.Text = "X : " + e.X.ToString() + ", Y : " + e.Y.ToString();
            if (Down)
            {
                if (wher == 0)
                {
                    try
                    {
                        Graphics.DrawLine(new Pen(color_, int.Parse(textBox4.Text)), last_x, last_y, e.X, e.Y);
                        PictureBox1.Image = flag;
                        PictureBox1.Refresh();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error in Converting Data!");
                    }
                    

                }
                else if (wher == 10)
                {
                    try
                    {
                        Graphics.DrawEllipse(new Pen(bgcolor, int.Parse(textBox5.Text)), e.X, e.Y, 10, 10);
                        PictureBox1.Image = flag;
                        PictureBox1.Refresh();
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Error in Converting Data!");
                    }
                }
            }
            last_x = e.X;
            last_y = e.Y;
        }

        void DownM(object sender, MouseEventArgs e)
        {
            if (wher == 1) {
                try
                {
                    int h = int.Parse(T1.Text);
                    int w = int.Parse(T2.Text);


                    Graphics.FillEllipse(brush, e.X, e.Y, h, w);
                    PictureBox1.Refresh();
                    wher = 0;
                }
                catch (Exception)
                {
                    MessageBox.Show("Error in Converting Data!");
                }
            }
            else if (wher == 2)
            {
                X_ = e.X;
                Y_ = e.Y;
                wher = 3;
            }
            else if (wher == 3) {
                Graphics.DrawLine(pen, X_, Y_, e.X, e.Y);
                PictureBox1.Refresh();
                wher = 0;
            }
            else if (wher == 4)
            {
                X_ = e.X;
                Y_ = e.Y;
                wher = 5;
            }
            else if (wher == 5)
            {

                if (X_ > e.X || Y_ > e.Y)
                   Graphics.DrawRectangle(pen, X_, Y_, e.X, e.Y);
                else
                    Graphics.DrawRectangle(pen, X_, Y_, e.X - X_, e.Y - Y_);

                PictureBox1.Refresh();
                wher = 0;
            }
            else if (wher == 6)
            {
                X_ = e.X;
                Y_ = e.Y;
                wher = 7;
            }
            else if (wher == 7)
            {
                Graphics.DrawLine(pen, X_, Y_, X_, e.Y);
                PictureBox1.Refresh();
                wher = 0;
            }
            else if (wher == 8)
            {
                X_ = e.X;
                Y_ = e.Y;
                wher = 9;
            }
            else if (wher == 9)
            {
                Graphics.DrawLine(pen, X_, Y_, e.X, Y_);
                PictureBox1.Refresh();
                wher = 0;
            }

            Down = true;
        }

        void MouseUp(object sender, EventArgs e)
        {
            Down = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            PictureBox1 = new PictureBox();
            PictureBox1.Size = new Size(this.Width, this.Height);
            PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Normal;
            this.Controls.Add(PictureBox1);
            PictureBox1.Cursor = System.Windows.Forms.Cursors.Cross;
            PictureBox1.MouseMove += new MouseEventHandler(Click);
            PictureBox1.MouseDown += new MouseEventHandler(DownM);
            PictureBox1.MouseUp += new MouseEventHandler(MouseUp);
            flag = new Bitmap(this.Width, this.Height);
            Graphics = Graphics.FromImage(flag);
            Graphics.FillRectangle(black, 0, 0, this.Width, this.Height);
            PictureBox1.Image = flag;
            PictureBox1.Refresh();
            T1 = textBox1;
            T2 = textBox2;
            PictureBox1.ContextMenuStrip = this.contextMenuStrip1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ColorDialog color = new ColorDialog();
            color.ShowDialog();
            brush = new SolidBrush(color.Color);
            pen = new Pen(color.Color);
            color_ = color.Color;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (DialogResult.Yes == MessageBox.Show("Cls ? ", "cl", MessageBoxButtons.YesNo))
            {
                flag = new Bitmap(this.Width, this.Height);
                Graphics = Graphics.FromImage(flag);
                Graphics.FillRectangle(black, 0, 0, this.Width, this.Height);
                PictureBox1.Image = flag;
                PictureBox1.Refresh();
                label2.BackColor = bgcolor;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            wher = 10;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            wher = 1;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.FileName = "Ra.png";
            dialog.Filter = "png|*.png";
            if (dialog.ShowDialog() == DialogResult.OK)
            {

                flag.Save(dialog.FileName, System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            wher = 2;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            wher = 0;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            wher = 4;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            wher = 6;
        }

        private void button10_Click(object sender, EventArgs e)
        {
            wher = 8;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            ColorDialog color = new ColorDialog();
            color.ShowDialog();
            black = new SolidBrush(color.Color);
            bgcolor = color.Color;
        }

        private void label2_Click(object sender, EventArgs e)
        {
            

        }

        private void button12_Click(object sender, EventArgs e)
        {

            try
            {
                float aa = float.Parse(textBox3.Text);
                Graphics.RotateTransform(aa);
                PictureBox1.Image = flag;
                PictureBox1.Refresh();
            }
            catch (Exception)
            {

                MessageBox.Show("Error in Converting Data!");
            }
        }
    }
}
